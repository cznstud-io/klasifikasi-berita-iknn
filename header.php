
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Halaman Pengguna -  <?= $config['nama_website'] ?></title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/site.css" rel="stylesheet">
    <?= $vws->get_inline() ?>
</head>

<body>
  <div class="wrapper">
    <nav id="sidebar">
      <div id="sidebar-header">
        <h3><?= $config['nama_website'] ?></h3>
      </div>
      
      <ul class="list-unstyled components">
        <li <?= ($config['hal_aktif'] == 'home')?'class="active"':'' ?>><a href="dash.php"><i class="fa fa-home" style="font-size: 1.5em"></i>&nbsp;&nbsp;Beranda</a></li>
        <li <?= ($config['hal_aktif'] == 'latih')?'class="active"':'' ?>><a href="lihat_latih.php"><i class="fa fa-database" style="font-size: 1.5em"></i>&nbsp;&nbsp;<?= hak_akses([1])?"Kelola":"Lihat"; ?> Data Latih</a></li>
        <li <?= ($config['hal_aktif'] == 'uji')?'class="active"':'' ?>><a href="lihat_uji.php"><i class="fa fa-database" style="font-size: 1.5em"></i>&nbsp;&nbsp;<?= hak_akses([1])?"Kelola":"Lihat"; ?> Data Uji</a></li>
        <li <?= ($config['hal_aktif'] == 'prediksi')?'class="active"':'' ?>><a href="prediksi.php"><i class="fa fa-twitter" style="font-size: 1.5em"></i>&nbsp;&nbsp;Klasifikasi Tweet</a></li>
      </ul>
    </nav>
    <div id="content">
    <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn" style="margin-left:0px">
    <i class="glyphicon glyphicon-align-left"></i>
    </button>
    <h2><?= $config['judul_sub_halaman'] ?></h2>
    <hr style="border: 2px solid #5a5a5a" />