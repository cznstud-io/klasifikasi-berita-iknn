<?php
/**
 * Improved K-Nearest Neighbors (IKNN)
 * ** Algoritma IKNN untuk pemrosesan data teks. Belum diuji coba
 * ** untuk memproses bentuk data lain (harusnya sih bisa aja).
 *
 * Parameter Inisialisasi: [int $K_value, array $class_label]
 * ** $K_value     = Nilai K awal. Harus dalam integer
 * ** $class_label = Label kelas yang digunakan dalam data. 
 * 
 * Daftar Fungsi:
 * - fit(array $samples, array $sample_labels)
 *   Memuat sample latih ke model IKNN. Jumlah data pada $samples
 *   harus sama dengan jumlah data $sample_labels. 
 *   Return Value: tidak ada
 *  
 * - predict(array $test_samples)
 *   Memprediksi data sampel dan mengembalikan label kelas untuk
 *   setiap data sampel. Jumlah atribut pada data sampel uji harus 
 *   sama dengan jumlah atribut pada data sampel latih.
 *   Return Value: Hasil prediksi setiap data sample uji dalam array
 *  
 * Daftar Atribut:
 * - array $improvedK     : mengembalikan nilai K baru untuk setiap
 *                          kelas
 * - array $class_label   : mengembalikan label untuk setiap kelas
 * - array $probability   : mengembalikan skor probabilitas data
 *                          sampel uji terakhir pada setiap kelas
 * - array $predict_labels: mengembalikan label hasil prediksi terakhir
 * 
 *
 * Dibuat oleh: cynetzhan / Hafizhan Shidqi
 * Versi      : 0.1
 * Lisensi    : MIT
 * The MIT License (MIT)
 * Copyright (c) 2018, Hafizhan Shidqi
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

class IKNN {
 private $weight;
 private $weightVector = array('samples'=>array(), 'test_samples'=>array());
 private $samples;
 private $sample_labels;
 public $similarity;
 private $K;
 public $improvedK;
 public $class_label;
 public $probability;
 public $predict_labels;
 
 function __construct($K_value, $class_label){
  $this->K = $K_value;
  $this->class_label = $class_label;
 }
 
 private function _safediv($a, $b){
  if($b != 0){
   return $a / $b;
  } else {
   return 0;
  }
 }
 
 public function _weight($test_samples){
  $this->weight = array_fill(0,count($test_samples),array_fill(0,count($this->samples),0));
  foreach($test_samples as $key_row_test=>$row_test){
   //$this->_verboselog($key_row_test." => ",false,false);
   foreach($this->samples as $key_row_sample=>$row_sample){
    $this->weight[$key_row_test][$key_row_sample] = array_map(function($a,$b) {if($a != 0 || $b != 0) return $a*$b;}, $row_sample, $row_test);
    //$this->_verboselog(".",false,false);
    $this->weight[$key_row_test][$key_row_sample] = array_sum($this->weight[$key_row_test][$key_row_sample]);
   }
   //$this->_verboselog("..OK",false,true);
  }
  //$this->_verboselog(var_dump($this->weight)); 
  return $this->weight;
 }
 
 private function _weightVector($samples){
  $weightVector = array();
  foreach($samples as $key_row=>$row){
   $weightVector[$key_row] = 0;
   foreach($row as $key_attr=>$attr){
    $weightVector[$key_row] += $attr**2;
   }
   $weightVector[$key_row] = sqrt($weightVector[$key_row]);
  }
  return $weightVector;
 }
 
 private function _similarity(){
  foreach($this->weightVector['samples'] as $key_sample=>$sample){
   foreach($this->weightVector['test_samples'] as $key_test=>$test){
    $this->similarity[$key_test][$key_sample] = $this->_safediv($this->weight[$key_test][$key_sample],$test*$sample);
   }
  }
  return $this->similarity;
 }
 
 private function _improvedK(){
  $data_per_class = [];
  foreach($this->class_label as $class=>$lbl){
   $data_per_class[$class] = count(array_keys($this->sample_labels, $class));
  }
  
  //echo var_dump($data_per_class);
  $K_new = array();
  foreach($this->class_label as $class=>$lbl){
   $K_new[$class] = round($this->K * $data_per_class[$class]/max($data_per_class));
  }
  $this->improvedK = $K_new;
  return $this->improvedK;
 }
 
 function fit($samples, $sample_labels){
  $this->samples = $samples;
  $this->sample_labels = $sample_labels;
  $this->weightVector['samples'] = $this->_weightVector($samples);
  //$this->class_label = array_unique($sample_labels);
  $this->_improvedK();
 }
 
 function set_K($K_value){
  $this->K = $K_value;
  $this->_improvedK();
 }
 
 private function _verboselog($msg, $with_time = true, $new_line= true){
  echo $msg;
  if($with_time){
   echo " - ".date('h:i:s');
  }
  if($new_line){
   echo "<br>";
  }
  ob_flush();
  flush();
 }
 
 function predict($test_samples){
  //$test_samples = is_array($test_samples)?$test_samples:array($test_samples);
  $this->_weight($test_samples);
  $this->weightVector['test_samples'] = $this->_weightVector($test_samples);
  $this->_similarity();

  $score = array();
  foreach($this->similarity as $key_uji=>$dok){
   arsort($dok); // urutkan nilai similaritas dari tinggi ke rendah
   $class_weight = array();
   foreach($this->class_label as $class=>$lbl){
    if($this->improvedK[$class] !== 0){
     $dok = array_slice($dok,0,$this->improvedK[$class],true);
     $class_weight[$class] = 0;
     foreach($dok as $key_smp=>$sim_weight){
      if($this->sample_labels[$key_smp] == $class){
       $class_weight[$class] += $sim_weight;
      }
     }
     $class_weight[$class] = $this->_safediv($class_weight[$class], array_sum($dok));
    } else {
     $class_weight[$class] = 0;
    }
    
   }
   $score[$key_uji] = $class_weight;
  }
  $this->probability = $score;
  foreach($score as $key_uji=>$uji){
   $key_label = array_search(max($uji), $uji);
   //$this->predict_labels[$key_uji] = $this->class_label[$key_label];
   $this->predict_labels[$key_uji] = (string)$key_label;
  }
  
  return $this->predict_labels;
 }
}