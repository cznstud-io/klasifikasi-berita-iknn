<?php
include_once('config/koneksi.php');
include_once('IKNN.php');
include_once('vendor/autoload.php');
include('preprocess.php');
use Phpml\FeatureExtraction\TokenCountVectorizer;
use Phpml\FeatureExtraction\TfIdfTransformer;
use Phpml\Tokenization\WordTokenizer;

$config['judul_sub_halaman'] = "<span class='fa fa-twitter'></span> Klasifikasi Tweet";
$config['hal_aktif'] = "prediksi";
//hak_akses([1],TRUE);
if (isset($_POST['isi_tweet'])){
 $query = mysqli_query($kon, "select * from data_latih");
 while($row = mysqli_fetch_assoc($query)){
  $data_latih['tweet'][] = $row['isi_tweet'];
  $data_latih['kelas'][] = $row['kelas'];
 }

 $data_uji['tweet'] = array($_POST['isi_tweet']);

 $latih_copy = preprocess($data_latih['tweet']);
 $uji_copy = preprocess($data_uji['tweet']);

 $vectorizer = new TokenCountVectorizer(new WordTokenizer());
 $vectorizer->fit($latih_copy);
 $vectorizer->transform($latih_copy);
 $vectorizer->transform($uji_copy);

 $merge_sample = array_merge($latih_copy, $uji_copy);
 $transformer = new TfIdfTransformer($merge_sample);
 $transformer->transform($latih_copy);
 $transformer->transform($uji_copy);
 
 $K = ($_POST['nilai_k']=='') ? 3 : $_POST['nilai_k'];
 $label = kelas();

 $model = new IKNN($K, $label);
 $model->fit($latih_copy, $data_latih['kelas']);
 $hasil = $model->predict($uji_copy);
 $skor_prob = $model->probability;
}
include('header.php');
?>
<div class="row">
 <div class="col-sm-6">
  <form method="POST" name="tweet" id="prediksi_param" class="form-horizontal">
    <div class="form-group">
     <div class="col-sm-4">
      <label class="control-label" for="isi_tweet">Isi Tweet</label>
     </div>
     <div class="col-sm-8">
      <textarea name="isi_tweet" class="form-control" id="isi_tweet" rows="10"><?= isset($_POST['isi_tweet'])?$_POST['isi_tweet']:'' ?></textarea>
     </div>
    </div>
    <div class="form-group">
     <div class="col-sm-4">
      <label class="control-label" for="nilai_k">Nilai K</label>
     </div>
     <div class="col-sm-8">
      <input type="number" name="nilai_k" class="form-control" id="nilai_k" value="<?= isset($_POST['nilai_k'])?$_POST['nilai_k']:'' ?>" />
     </div>
    </div>
    <div class="form-group">
     <div class="col-sm-offset-4 col-sm-8">
      <div class="hidden text-center" id="progress"><span class="fa fa-refresh fa-spin"></span> Mengklasifikasi Tweet...</div>
      <button type="button" id="btn_prediksi" class="form-control btn btn-login" name="simpan">Klasifikasi Tweet</button>
     </div>
    </div>
  </form>
 </div>
 <div class="col-sm-6">
  <div class="well">
   <div class="text-center">
    <strong>Hasil Klasifikasi</strong>
    <span style="font-weight:700;font-size:2em" id="hasil_prediksi"><br>---</span>
   </div>
   <table class="table table-striped table-bordered table-responsive">
    <tr>
     <th colspan="2" class="text-center">Skor Probabilitas</th>
    </tr>
    <?php foreach(kelas() as $key=>$label){ ?>
    <tr>
     <td><?= $label ?></td>
     <td id="skor_<?= $key ?>">---</td>
    </tr>
    <?php } ?>
   </table>
  </div>
 </div>
</div>
<?php
ob_start();
?>
<script>
function kelas(kls){
 var arr = ['Manusia dan Peristiwa','Politik','Olahraga'];
 return arr[kls];
}
</script>
<script>
$(document).ready(function() {
 $("#btn_prediksi").on('click', function() {
  $(this).attr('disabled','disabled');
  $("#progress").removeClass('hidden');
  $.ajax({
    url: 'ajax_prediksi.php',
    method: 'POST',
    data: $("#prediksi_param").serialize(),
    error: function() {},
    success: function(response) {
     console.log(response);
     $("#hasil_prediksi").html("<br>"+kelas(response.hasil));
     Object.keys(response.skor[0]).forEach(function(e) {
      $("#skor_"+e).html(response.skor[0][e]);
     });
    },
    complete: function() {
     $("#btn_prediksi").attr('disabled',null);
     $("#progress").addClass('hidden');
    }
  });
 });
});
</script>
<?php
$vws->set_inline(ob_get_clean());
include('footer.php');