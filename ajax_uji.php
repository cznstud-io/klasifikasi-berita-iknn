<?php
include_once('config/koneksi.php');
include_once('IKNN.php');
include_once('vendor/autoload.php');
include('preprocess.php');
use Phpml\FeatureExtraction\TokenCountVectorizer;
use Phpml\FeatureExtraction\TfIdfTransformer;
use Phpml\Tokenization\WordTokenizer;
use Phpml\Metric\Accuracy;
use Phpml\Metric\ClassificationReport;
$timeawal = time();
$K_value = [3,5,7];

$query = mysqli_query($kon, "select isi_bersih_tweet,kelas from data_latih");
while($row = mysqli_fetch_assoc($query)){
 $data_latih['tweet'][] = $row['isi_bersih_tweet'];
 $data_latih['kelas'][] = $row['kelas'];
}

$query = mysqli_query($kon, "select * from data_uji");
while($row = mysqli_fetch_assoc($query)){
 $data_uji['tweet'][] = $row['isi_tweet'];
 $data_uji['kelas'][] = $row['kelas'];
}
verbosejson(['progress'=>100, 'progress_msg'=>'Memuat Data...', 'latih_count'=>count($data_latih['tweet']), 'uji_count'=>count($data_uji['tweet']), 'K_value'=>$K_value]);

$latih_copy = $data_latih['tweet'];
$uji_copy = preprocess($data_uji['tweet']);

verbosejson(['progress'=>10, 'progress_msg'=>'Tokenisasi Data...' ]);

$vectorizer = new TokenCountVectorizer(new WordTokenizer());
$vectorizer->fit($latih_copy);
$vectorizer->transform($latih_copy);
$vectorizer->transform($uji_copy);

verbosejson(['progress'=>30, 'progress_msg'=>'Menghitung TF-IDF...' ]);

$merge_sample = array_merge($latih_copy, $uji_copy);
$transformer = new TfIdfTransformer($merge_sample);
$transformer->transform($latih_copy);
$transformer->transform($uji_copy);

$latih_copy = pad_one($latih_copy);
$uji_copy = pad_one($uji_copy);


$progress_per_persen = 70 / count($uji_copy); // sisa progress 70 persen lagi, dihitung besar langkah progressnya berdasar banyaknya data uji sampai progress 100 persen
$label = kelas();
$hasil_per_K = [];
$nilai_baru_K = [];
$model = new IKNN(3, kelas());
$model->fit($latih_copy, $data_latih['kelas']);
foreach($uji_copy as $key_uji=>$uji){
 foreach($K_value as $K){
  $model->set_K($K);
  $nilai_baru_K[$K] = $model->improvedK;
  $hasil_per_K[$key_uji][$K] = $model->predict(array($uji))[0];
 }
 verbosejson([
  'progress'=>round(30 + ($progress_per_persen * ($key_uji + 1))), 
  'progress_msg'=>'Mengklasifikasi data ke-'.($key_uji+1).'...', 
  'tweet'=>$data_uji['tweet'][$key_uji], 'kelas'=>$data_uji['kelas'][$key_uji], 'prediksi'=>$hasil_per_K[$key_uji] ], false, true);
}

$akurasi = [];
foreach($K_value as $K){
 //echo number_format(Accuracy::score($data_uji['kelas'], array_column($hasil_per_K, $K)) * 100,2);
 $report = new ClassificationReport($data_uji['kelas'], array_column($hasil_per_K, $K));
 $akurasi[$K] = array_map("numformat", $report->getAverage());
}

verbosejson(['progress'=>100, 'progress_msg'=>'Prediksi Selesai', 'akurasi_K'=>$akurasi, 'K_baru'=> $nilai_baru_K, 'time_elapsed'=> number_format((time() - $timeawal) / 60,2) ]);
