<?php
include('stemming.php');
function tokenize($text){
  $text = preg_replace("/((http)+(s)?:\/\/[^<>\s]+)/i", "", $text );
		$text = preg_replace("/[@]+([A-Za-z0-9-_]+)/", '', $text );
		$text = preg_replace("/[#]+([A-Za-z0-9-_]+)/", '', $text );
		$text = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $text );
		$text = preg_replace('/&#?[a-z0-9]{2,8};/i', '', $text );
  $text = str_replace(str_split('#"~!@$\%^&*()_+`=-<>?,./:{}[];\|'), ' ', $text);
  $text = str_replace("'", ' ', $text);
  return $text;
}

function stemming($text){
 $words = explode(" ",$text);
 foreach($words as $key=>$value){
  $words[$key] = NAZIEF($value);
 }
 return implode(" ", $words);
}

function remove_stopword($text){
 $stopwords = explode(" ", file_get_contents('file-stopword.txt'));
 $words = explode(" ", $text);
 $words_cleaned = array_diff($words, $stopwords);
 return implode(" ", $words_cleaned);
}

function preprocess($text_collection){
 foreach($text_collection as $key=>$value){
  $text_collection[$key] = remove_stopword(stemming(tokenize($value)));
 }
 return $text_collection;
}