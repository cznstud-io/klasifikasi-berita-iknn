<?php
include_once('config/koneksi.php');
$config['judul_sub_halaman'] = "<span class='fa fa-refresh'></span> Pengujian Data Latih";
$config['hal_aktif'] = "latih";
//hak_akses([1],TRUE);
ob_start();
?>
<link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
<link href="assets/css/jquery.dataTables_themeroller.min.css" rel="stylesheet"/>
<?php
$vws->set_inline(ob_get_clean());
include('header.php');
$vws->reset_inline();
?>
<div class="row">
 <div class="col-sm-12">
  <h3>Parameter Pengujian</h3>
  <form name="evaluasi" id="evaluasi" class="form-horizontal">
  <div class="form-group">
   <label class="col-sm-4" for="n_fold">Nilai K-Fold</label>
   <div class="col-sm-8">
    <input type="number" class="form-control" name="n_fold" id="n_fold" placeholder="Nilai Fold (hanya dalam bilangan bulat)" />
   </div>
  </div>
  <div class="form-group">
   <label class="col-sm-4" for="k_value">Nilai K (untuk IKNN)</label>
   <div class="col-sm-8">
    <input type="number" class="form-control" name="k_value" id="k_value" placeholder="Nilai K (hanya dalam bilangan bulat)" />
   </div>
  </div>
  <div class="form-group">
   <div class="col-sm-8 col-sm-offset-4">
    <button type="button" class="btn btn-login form-control" id="proses"><span class="fa fa-refresh"></span> Mulai Pengujian</button>
   </div>
  </div>
  </form>
 </div>
</div>

<div class="row" id="hasil">
 <div class="col-sm-12">
  <div class="well">
   <div class="text-center">
   <strong>Indikator Proses</strong>
   <div class="progress" style="margin-bottom:0px">
     <div class="progress-bar progress-bar-striped active" role="progressbar" id="prosespersen"
     aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
     </div>
   </div>
   <div id="prosespersen_text" style="margin-bottom: 15px">Siap Menjalankan Pengujian</div>
   </div>
   <table class="table table-striped table-responsive">
    <tr>
     <th>Waktu Mulai Pengujian</th>
     <td><span id="time_start">--</span></td>
     <th>Waktu Selesai Pengujian</th>
     <td><span id="time_finish">--</span> (<span id="time_elapsed">--</span> menit)</td>
    </tr>
    <tr>
     <th>Jumlah Data Latih</th>
     <td><span id="data_count">--</span></td>
     <th>Akurasi Rata-rata</th>
     <td><span id="avg_score">--</span>%</td>
    </tr>
   </table>
  </div>
  <h3>Hasil Akurasi</h3>
  <table class="table table-striped table-bordered" id="hasil_uji">
   <thead>
    <tr>
     <th>Fold Ke-</th>
     <th>Akurasi</th>
    </tr>
   </thead>
   <tbody>
   </tbody>
  </table>
 </div>
</div>
<?php
ob_start();
?>
<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/dataTables.bootstrap.min.js"></script>
<script>
function fold_generator(fold_value, data_count){
 if(fold_value < 2){
  return false;
 } else {
  var fold = Array();
  subsample_length = Math.ceil(data_count/fold_value);
  fold['subsample_length'] = subsample_length;
  fold['fold'] = Array.apply(null, Array(fold_value)).map(function(_,i) {return i * subsample_length});
  return fold;
 }
}
function appendResult(result){
 $("#hasil_uji>tbody").append("<tr><td>"+result.fold+"</td><td>"+ (result.accuracy * 100) +"%</td></tr>");
}
</script>
<script>
 $(document).ready(function() {
  $("#bukukita").DataTable();
  $("#proses").on('click', function() {
   $("#hasil").removeClass('hidden');
   $(this).attr('disabled','disabled');
   time_start = new Date();
   $("#time_start").text(time_start.toTimeString().split(' ')[0]);
   $.ajax({
    url: 'ajax_crossfold.php',
    method: 'GET',
    data: $("#evaluasi").serialize(),
    xhrFields: {
     onprogress: function(e){
      var response = e.currentTarget.response;
      console.log(this.readyState);
      allMessage = response.split("\n");
      lastMessage = JSON.parse(allMessage[allMessage.length - 2]);
      console.log(lastMessage);
      if(lastMessage.message.hasOwnProperty('data_count')){
       $("#data_count").text(lastMessage.message.data_count);
      }
      if(lastMessage.message.hasOwnProperty('result')){
       appendResult(lastMessage.message.result);
      }
      if(lastMessage.message.hasOwnProperty('avg_score')){
       $("#avg_score").text(lastMessage.message.avg_score);
       $("#time_elapsed").text(lastMessage.message.time_elapsed);
       timefinish = new Date();
       $("#time_finish").text(timefinish.toTimeString().split(' ')[0]);
      }
      $("#prosespersen_text").html(lastMessage.message.progress_msg);
      $("#prosespersen").css('width',lastMessage.message.progress+'%');
     },
     onreadystatechange: function(){
      if(this.readyState == 4){
       $("#prosespersen").removeClass('active');
       console.log("DONE!");
      }
     }
    },
    complete: function(e,s){
     if(s == 'success'){
      $("#prosespersen_text").html("Pengujian Selesai");
     } else if(s == 'error' || s == 'parsererror') {
      $("#prosespersen_text").html("ERROR! Terdapat masalah koneksi ke server");
     }
    }
   });
  });
 });
</script>
<?php
$vws->set_inline(ob_get_clean());
include('footer.php');

