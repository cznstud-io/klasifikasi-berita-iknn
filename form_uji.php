<?php
include_once('config/koneksi.php');
$config['judul_sub_halaman'] = "<span class='fa fa-database'></span> Tambah Data Uji";
$config['hal_aktif'] = "uji";
//hak_akses([1],TRUE);
if(isset($_GET['edit'])){
 $config['judul_sub_halaman'] = "<span class='fa fa-database'></span> Ubah Data Uji";
 $query = mysqli_query($kon, "select * from data_uji where id='".$_GET['edit']."'");
 $row = mysqli_fetch_assoc($query);
} else if (isset($_POST['id'])){
 if($_POST['id'] === ''){
  $query = mysqli_query($kon, "insert into data_uji values('','".$_POST['isi_tweet']."','".$_POST['kelas']."')");
  if($query){
   echo "<script>alert('Data Uji berhasil ditambahkan!');\n document.location = 'lihat_uji.php'</script>";
  } else {
   echo "<script>alert('Terdapat Kesalahan Penambahan. Kode Error: '".mysqli_error($kon).");\n document.location = 'lihat_uji.php'</script>";
  }
 } else {
  $query = mysqli_query($kon,"update data_uji set isi_tweet='".htmlspecialchars($_POST['isi_tweet'])."', kelas='".$_POST['kelas']."' where id='".$_POST['id']."'");
  if($query){
   echo "<script>alert('Data Uji berhasil diperbarui');\n document.location = 'lihat_uji.php'</script>";
  } else {
   echo "<script>alert('Terdapat Kesalahan dalam pembaruan data. Kode Error: '".mysqli_error($kon).");\n document.location = 'lihat_uji.php'</script>";
  }
 }
}
include('header.php');
?>
<form method="POST" action="form_uji.php" name="tweetuji" class="form-horizontal">
  <input type="hidden" name="id" value="<?= isset($row['id'])?$row['id']:'' ?>" />
  <div class="form-group">
   <div class="col-sm-4">
    <label class="control-label" for="id_kategori">Isi Tweet</label>
   </div>
   <div class="col-sm-8">
    <textarea name="isi_tweet" class="form-control"><?= isset($row['isi_tweet'])?htmlspecialchars_decode($row['isi_tweet']):'' ?></textarea>
   </div>
  </div>
  <div class="form-group">
   <div class="col-sm-4">
    <label class="control-label" for="nama_kategori">Jenis Tweet</label>
   </div>
   <div class="col-sm-8">
    <select name="kelas" class="form-control">
     <option selected disabled>Pilih Jenis...</option>
     <?php foreach(kelas() as $key=>$value){ ?>
     <option value="<?= $key ?>" <?= isset($row['kelas'])?($row['kelas']==$key?'selected':''):'' ?>><?= $value ?></option>
     <?php } ?>
    </select>
   </div>
  </div>
  
  <div class="form-group">
   <div class="col-sm-12">
    <button type="submit" class="form-control btn btn-login" name="simpan">Simpan</button>
   </div>
  </div>
</form>
<?php
include('footer.php');