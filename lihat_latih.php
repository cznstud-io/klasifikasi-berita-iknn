<?php
include_once('config/koneksi.php');
$config['judul_sub_halaman'] = "<span class='fa fa-database'></span> Kelola Data Latih";
$config['hal_aktif'] = "latih";
//hak_akses([1],TRUE);
ob_start();
?>
<link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
<link href="assets/css/jquery.dataTables_themeroller.min.css" rel="stylesheet"/>
<?php
$vws->set_inline(ob_get_clean());
include('header.php');
$vws->reset_inline();
?>
<div class="col-sm-12">
 <div class="btn-group" style="margin:25px 5px">
  <a href="form_latih.php" class="btn btn-primary btn-md"><span class="fa fa-plus"></span> Tambah Data Latih</a>
  <!--a href="prediksi_crossfold.php" class="btn btn-primary btn-md"><span class="fa fa-refresh"></span> Proses Uji Crossfold</a-->
 </div>
 <table class="table table-striped table-bordered table-responsive" id="bukukita">
  <thead>
   <tr>
    <th>Tweet</th>
    <th>Jenis Berita</th>
    <th>Aksi</th>
   </tr>
  </thead>
  <tbody>
   <?php 
   $query = mysqli_query($kon, "select * from data_latih");
   if(mysqli_num_rows($query) !== 0){
    while($row=mysqli_fetch_assoc($query)){ ?>
    <tr>
     <td><?= $row['isi_tweet'] ?></td>
     <td><?= kelas($row['kelas']) ?></td>
     <td><a href="form_latih.php?edit=<?= $row['id'] ?>"><span class="fa fa-pencil"></span> Ubah</a> | <a href="hapus.php?table=data_latih&id=<?= $row['id'] ?>" onclick="return confirm('Hapus data ini?');"><span class="fa fa-close"></span> Hapus</a></td>
    </tr>
    <?php }
    } else {
     ?>
     <tr>
      <td colspan="5">Tidak Ada Data</td>
     </tr>
    <?php } ?>
  </tbody>
 </table>
</div>
<?php
ob_start();
?>
<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/dataTables.bootstrap.min.js"></script>
<script>
 $(document).ready(function() {
  $("#bukukita").DataTable();
 });
</script>
<?php
$vws->set_inline(ob_get_clean());
include('footer.php');