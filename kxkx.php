<?php
include_once('config/koneksi.php');
include_once('IKNN.php');
include_once('vendor/autoload.php');
include('preprocess.php');
use Phpml\FeatureExtraction\TokenCountVectorizer;
use Phpml\FeatureExtraction\TfIdfTransformer;
use Phpml\Tokenization\WordTokenizer;
use Phpml\Metric\Accuracy;
use Phpml\CrossValidation\RandomSplit;
use Phpml\CrossValidation\StratifiedRandomSplit;

$timeawal = time();

$query = mysqli_query($kon, "select isi_bersih_tweet,kelas from data_latih");
while($row = mysqli_fetch_assoc($query)){
 $data_raw['tweet'][] = $row['isi_bersih_tweet'];
 $data_raw['kelas'][] = $row['kelas'];
}

 $vectorizer = new TokenCountVectorizer(new WordTokenizer());
 $vectorizer->fit($data_raw['tweet']);
 $vectorizer->transform($data_raw['tweet']);
 
 echo "DATA VECTORIZED - ";
 echo time() - $timeawal;
 echo "s<br>";
 ob_flush();
 flush();
 
 //$merge_sample = array_merge($data_raw['tweet'], $data_raw['tweet']);
 $transformer = new TfIdfTransformer($data_raw['tweet']);
 $transformer->transform($data_raw['tweet']);
 
 $model = new IKNN(3, kelas());
 $model->fit($data_raw['tweet'], $data_raw['kelas']);
 $weight = $model->_weight($data_raw['tweet']);
 
 echo "DATA WEIGHTED - ";
 echo time() - $timeawal;
 echo "s<br>";
 echo var_dump($weight);
 ob_flush();
 flush();
 
 