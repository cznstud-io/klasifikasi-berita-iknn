<?php
include_once('config/koneksi.php');
include_once('IKNN.php');
include_once('vendor/autoload.php');
include('preprocess.php');
use Phpml\FeatureExtraction\TokenCountVectorizer;
use Phpml\FeatureExtraction\TfIdfTransformer;
use Phpml\Tokenization\WordTokenizer;
if (isset($_POST['isi_tweet'])){
 $query = mysqli_query($kon, "select * from data_latih");
 while($row = mysqli_fetch_assoc($query)){
  $data_latih['tweet'][] = $row['isi_tweet'];
  $data_latih['kelas'][] = $row['kelas'];
 }

 $data_uji['tweet'] = array($_POST['isi_tweet']);

 $latih_copy = preprocess($data_latih['tweet']);
 $uji_copy = preprocess($data_uji['tweet']);

 $vectorizer = new TokenCountVectorizer(new WordTokenizer());
 $vectorizer->fit($latih_copy);
 $vectorizer->transform($latih_copy);
 $vectorizer->transform($uji_copy);

 $merge_sample = array_merge($latih_copy, $uji_copy);
 $transformer = new TfIdfTransformer($merge_sample);
 $transformer->transform($latih_copy);
 $transformer->transform($uji_copy);
 
 $latih_copy = pad_one($latih_copy);
 $uji_copy = pad_one($uji_copy);
 
 $K = ($_POST['nilai_k']=='') ? 3 : $_POST['nilai_k'];
 $label = kelas();

 $model = new IKNN($K, $label);
 $model->fit($latih_copy, $data_latih['kelas']);
 $hasil = $model->predict($uji_copy);
 $skor_prob = $model->probability;
 
 $json = array(
  'hasil' => $hasil,
  'skor' => $skor_prob
 );
 header('Content-Type: text/json');
 echo json_encode($json);
}