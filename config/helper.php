<?php

function redirect($url){
 header('Location: '.$url, TRUE, 302);
}

function filter_res($identifier, $exception = array()){
  $result = array();
  foreach($identifier as $key=>$value){
   if(array_search($key,$exception) === FALSE){
    $result[$key] = htmlspecialchars($value, ENT_QUOTES);
   } else {
    $result[$key] = $value;
   }
  }
  return $result;
}

function numformat($n){
 return number_format($n,3);
}

function tanggal($dt,$with_timestamp=false){
 //format harus yyyy-mm-dd
 $bulan=array(
  "00" => "N/A",
  "01" => "Januari",
  "02" => "Februari",
  "03" => "Maret",
  "04" => "April",
  "05" => "Mei",
  "06" => "Juni",
  "07" => "Juli",
  "08" => "Agustus",
  "09" => "September",
  "10" => "Oktober",
  "11" => "November",
  "12" => "Desember"
 );
 $date=explode("-",$dt);
 $tahun=substr($date[2],0,2); //fix date with timestamp format
 $tanggal=$tahun." ".$bulan[$date[1]]." ".$date[0];
 if($with_timestamp){
  $tanggal .= " ".substr($date[2],3);
 }
 return $tanggal;
}

function hak_akses($level=array(), $redirect=false){
 $accepted = 0;
 if(isset($_SESSION['level'])){
  foreach($level as $lv){
   if($_SESSION['level'] == $lv){
    $accepted += 1;
   }
  }
 }
 if($accepted > 0){
  return TRUE;
 } else {
  if($redirect){
   echo "<script>";
   echo "alert('Anda tidak memiliki akses ke halaman ini');";
   echo "document.location.href = 'login.php';";
   echo "</script>";
  }
  return FALSE;
 }
}

function fold_generator($fold_value, $data_count){
 if($fold_value < 2){
  return FALSE;
 } else {
  $subsample_length = ceil($data_count/$fold_value);
  $fold['subsample_length'] = $subsample_length;
  $fold['fold'] = array();
  foreach(range(0,$data_count,$subsample_length) as $fd){
   if(count($fold['fold']) < $fold_value){
    $fold['fold'][] = $fd;
   } else {
    break;
   }
  }
  return $fold;
 }
}

function verboselog($msg, $with_time = true, $new_line= true){
  echo $msg;
  if($with_time){
   echo " - ".date('h:i:s');
  }
  if($new_line){
   echo "<br>";
  }
  ob_flush();
  flush();
}

function verbosejson($msg, $with_time = false, $delay = true, $flush_right_away = true){
  $json = array();
  $json['message'] = $msg;
  if($with_time){
   $json['time'] = date('H:i:s');
  }
  echo json_encode($json);
  echo "\n";
  if($flush_right_away){
   ob_flush();
   flush();
  }
  if($delay){
   sleep(1);
  }
 }

function level_desc($lvl){
 $arr = array(1=>"Administrator","Anggota Perpustakaan","Kepala Pustaka");
 return $arr[$lvl];
}

function kelas($kls = ''){
 $label = ['Manusia dan Peristiwa','Politik','Olahraga'];
 return ($kls === '')?$label:$label[$kls];
}

function beda_hari($tgl1, $tgl2, $format = '%a'){
 $tgl1 = date_create($tgl1);
 $tgl2 = date_create($tgl2);
 $interval = date_diff($tgl1, $tgl2);
 return $interval->format($format);
}

function pad_one($data){
  foreach($data as $key=>$val){
    $arrhasil = [];
    foreach($val as $rval){
      $arrhasil[] =  $rval+1;
    }
    $data[$key] = $arrhasil;
  }
  return $data;
}

class ViewAsset{
 public $inlinejs = [];
 
 function set_inline($content, $index=0){
  if($index===0){
   $this->inlinejs[] = $content;
  } else {
   $this->inlinejs[$index] = $content;
  }
  
 }
 
 function get_inline($index=0){
  if($index===0){
   return implode('\n', $this->inlinejs);
  } else {
   if(is_array($index)){
    $return = array();
    foreach($index as $idx){
     $return = array_merge($return, $this->inlinejs[$idx]);
    }
   } else {
    $return = isset($this->inlinejs[$index])?$this->inlinejs[$index]:'';
   }
   return $return;
  }
 }
 
 function start_block($index){
  $this->inlinejs[$index] = "";
  ob_start();
 }
 
 function end_block($index){
  $this->inlinejs[$index] = ob_get_clean();
 }
 
 function reset_inline(){
  $this->inlinejs = [];
 }
}

$vws = new ViewAsset();
 ?>