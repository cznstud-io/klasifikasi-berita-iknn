<?php
include('vendor/autoload.php');
include('IKNN.php');
include('preprocess.php');
use Phpml\FeatureExtraction\TokenCountVectorizer;
use Phpml\FeatureExtraction\TfIdfTransformer;
use Phpml\Tokenization\WordTokenizer;

$label = ['negatif','netral','positif'];

/*$samples = [
    'Halo! Selamat ya atas kelulusannya!',
    'Kemana saja kalian pergi',
    'diam kalau mau selamat',
];
$samples_label = [2,1,0];
$tester = ['mau pergi ke tempat selamat nih'];
*/
$handle = fopen('cleaning.csv','r');
$count = 0;
while( ($data = fgetcsv($handle, 0, ';')) !== FALSE ){
 if($count!=0){
  if($count!=16){
   $samples[] = $data[0];
   $samples_label[] = array_search(strtolower($data[1]), $label);
  } else {
   $tester[] = $data[0];
  }
 }
 $count++;
}
fclose($handle);

echo var_dump($samples);
foreach($samples as $key=>$value){
 $samples[$key] = remove_stopword(stemming(tokenize($value)));
}
echo var_dump($samples);
echo var_dump($samples_label);

$samples_copy = $samples;

$vectorizer = new TokenCountVectorizer(new WordTokenizer());

$vectorizer->fit($samples);
$vectorizer->transform($samples_copy);
$vectorizer->transform($tester);

echo var_dump($vectorizer->getVocabulary());
echo var_dump($samples_copy);
echo var_dump($tester);

$tfidf_samples = array_merge($samples_copy, $tester);

$transformer = new TfIdfTransformer($tfidf_samples);
$transformer->transform($samples_copy);
$transformer->transform($tester);

echo var_dump($samples_copy);
echo var_dump($tester);

$model = new IKNN(3, $label);
$model->fit($samples_copy, $samples_label);
echo var_dump($model->predict($tester));
echo var_dump($model->improvedK);
echo var_dump($model->class_label);
echo var_dump($model->probability);


/*
// mendapatkan bobot teks uji
$bobot = array();
foreach($tester as $key_dok_test=>$dok_test){
 foreach($samples_copy as $key_dok_sample=>$dok_sample){
  $bobot[$key_dok_test][$key_dok_sample] = array_map(function($a,$b) {return $a*$b;}, $dok_sample, $dok_test);
  $bobot[$key_dok_test][$key_dok_sample] = array_sum($bobot[$key_dok_test][$key_dok_sample]);
 }
}
echo "Bobot";
echo var_dump($bobot);

// mendapatkan panjang vektor
$panjang_vektor = array('latih'=>array(), 'uji'=>array());
foreach($samples_copy as $key_dok=>$dok){
 $panjang_vektor['latih'][$key_dok] = 0;
 foreach($dok as $key_kata=>$kata){
  $panjang_vektor['latih'][$key_dok] += $kata**2;
 }
 $panjang_vektor['latih'][$key_dok] = sqrt($panjang_vektor['latih'][$key_dok]);
}

foreach($tester as $key_dok=>$dok){
 $panjang_vektor['uji'][$key_dok] = 0;
 foreach($dok as $key_kata=>$kata){
  $panjang_vektor['uji'][$key_dok] += $kata**2;
 }
 $panjang_vektor['uji'][$key_dok] = sqrt($panjang_vektor['uji'][$key_dok]);
}
echo var_dump($panjang_vektor);


// mencari kedekatan (cosine similarity)
$similarity = array();
foreach($panjang_vektor['latih'] as $key_sample=>$sample){
 foreach($panjang_vektor['uji'] as $key_uji=>$uji){
  $similarity[$key_uji][$key_sample] = safe_div($bobot[$key_uji][$key_sample],$uji*$sample);
 }
}
echo var_dump($similarity);

// mencari K baru untuk masing-masing kelas
$jumlah_data_perkelas = [];

foreach($label as $class=>$lbl){
 $jumlah_data_perkelas[$class] = count(array_keys($samples_label, $class));
}

$K_baru = array();
foreach($label as $class=>$lbl){
 $K_baru[$class] = $K_value * $jumlah_data_perkelas[$class]/max($jumlah_data_perkelas);
}
echo var_dump($jumlah_data_perkelas);
echo var_dump($K_baru);


// mencari probabilitas setiap kelas dari teks uji. perhitungan skor di setiap kelas menggunakan masing2 K baru
$score = array();
foreach($similarity as $key_uji=>$dok){
 arsort($dok); // urutkan nilai similaritas dari tinggi ke rendah
 //$dok = array_slice($dok,0,$K_value,true); // ambil K nilai pertama
 $class_weight = array();
 foreach($label as $class=>$lbl){
  if($K_baru[$class] !== 0){
   $dok = array_slice($dok,0,$K_baru[$class],true);
   $class_weight[$class] = 0;
   foreach($dok as $key_smp=>$sim_weight){
    if($samples_label[$key_smp] == $class){
     $class_weight[$class] += $sim_weight;
    }
   }
   $class_weight[$class] /= array_sum($dok);
  } else {
   $class_weight[$class] = 0;
  }
  
 }
 $score[$key_uji] = $class_weight;
}

echo var_dump($score);



#$classifier = new KNearestNeighbors($k=3);
#$classifier->train($samples,['asik','gakasik','asik']);
#echo var_dump($classifier->predict($samples));

*/



