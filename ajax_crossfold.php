<?php
include_once('config/koneksi.php');
include_once('IKNN.php');
include_once('vendor/autoload.php');
include('preprocess.php');
use Phpml\FeatureExtraction\TokenCountVectorizer;
use Phpml\FeatureExtraction\TfIdfTransformer;
use Phpml\Tokenization\WordTokenizer;
use Phpml\Metric\Accuracy;
use Phpml\CrossValidation\RandomSplit;
use Phpml\CrossValidation\StratifiedRandomSplit;
$timeawal = time();
$fold_value = $_GET['n_fold'];
$K_value = $_GET['k_value'];

$query = mysqli_query($kon, "select isi_bersih_tweet,kelas from data_latih");
while($row = mysqli_fetch_assoc($query)){
 $data_raw['tweet'][] = $row['isi_bersih_tweet'];
 $data_raw['kelas'][] = $row['kelas'];
}

verbosejson(['progress'=>100, 'progress_msg'=>'Memuat Data...', 'data_count'=>count($data_raw['tweet'])]);

$fold = fold_generator($fold_value, count($data_raw['tweet']));
foreach($fold['fold'] as $count_fold=>$fd){
 // $data_uji akan dihilangkan sebagiannya untuk dijadikan data latih dan sisanya data uji
 $data_latih = $data_raw;
 $data_uji['tweet'] = array_splice($data_latih['tweet'], $fd, $fold['subsample_length']);
 $data_uji['kelas'] = array_splice($data_latih['kelas'], $fd, $fold['subsample_length']);
  
 verbosejson(['progress'=>10, 'progress_msg'=>'[Fold '.($count_fold+1).'] Tokenisasi...']);
 $vectorizer = new TokenCountVectorizer(new WordTokenizer());
 $vectorizer->fit($data_latih['tweet']);
 $vectorizer->transform($data_latih['tweet']);
 $vectorizer->transform($data_uji['tweet']);

 verbosejson(['progress'=>30, 'progress_msg'=>'[Fold '.($count_fold+1).'] Menghitung TF-IDF...']);
 $merge_sample = array_merge($data_latih['tweet'], $data_uji['tweet']);
 $transformer = new TfIdfTransformer($merge_sample);
 $transformer->transform($data_latih['tweet']);
 $transformer->transform($data_uji['tweet']);
 $data_latih['tweet'] = pad_one($data_latih['tweet']);
 $data_uji['tweet'] = pad_one($data_uji['tweet']);
 
 verbosejson(['progress'=>60, 'progress_msg'=>'[Fold '.($count_fold+1) .'] Perhitungan IKNN...']);
 $model = new IKNN($K_value, kelas());
 $model->fit($data_latih['tweet'], $data_latih['kelas']);
 $hasil_fold[$count_fold] = Accuracy::score($data_uji['kelas'], $model->predict($data_uji['tweet']));
 
 verbosejson(['progress'=>100, 'progress_msg'=>'[Fold '.($count_fold+1) .'] Perhitungan Selesai', 'result'=>['fold'=>$count_fold+1, 'accuracy'=>number_format($hasil_fold[$count_fold],2)] ]);
}

$skor_avg = array_sum($hasil_fold) / $fold_value;
verbosejson(['progress'=>100, 'progress_msg'=>'Pengujian Selesai', 'avg_score'=> number_format($skor_avg * 100,2), 'time_elapsed'=> (time() - $timeawal) / 60]);
//echo var_dump($hasil_fold);
//echo $skor_avg;

/*

$latih_copy = preprocess($data_latih['tweet']);
$uji_copy = preprocess($data_uji['tweet']);



$merge_sample = array_merge($latih_copy, $uji_copy);
$transformer = new TfIdfTransformer($merge_sample);
$transformer->transform($latih_copy);
$transformer->transform($uji_copy);

$K_value = [3,5,9,11,15];
$label = kelas();
$hasil_per_K = [];
foreach($K_value as $K){
 $model = new IKNN($K, $label);
 $model->fit($latih_copy, $data_latih['kelas']);
 $hasil_per_K[$K] = $model->predict($uji_copy);
}
*/