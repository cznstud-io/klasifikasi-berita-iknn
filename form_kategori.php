<?php
include_once('config/koneksi.php');
$config['judul_sub_halaman'] = "Tambah Kategori";
$config['hal_aktif'] = "kategori";
hak_akses([1],TRUE);
if(isset($_GET['edit'])){
 $config['judul_sub_halaman'] = "Ubah Kategori";
 $query = mysqli_query($kon, "select * from kategori where id='".$_GET['edit']."'");
 $row = mysqli_fetch_assoc($query);
} else if (isset($_POST['id'])){
 if($_POST['id_lama'] === ''){
  $query = mysqli_query($kon, "insert into kategori values('".$_POST['id']."','".$_POST['nama_kategori']."')");
  if($query){
   echo "<script>alert('Kategori berhasil ditambahkan!');\n document.location = 'lihat_kategori.php'</script>";
  } else {
   echo "<script>alert('Terdapat Kesalahan Penambahan. Kode Error: '".mysqli_error($kon).");\n document.location = 'lihat_kategori.php'</script>";
  }
 } else {
  $query = mysqli_query($kon,"update kategori set id='".$_POST['id']."',nama_kategori='".$_POST['nama_kategori']."' where id='".$_POST['id_lama']."'");
  if($query){
   echo "<script>alert('Kategori berhasil diperbarui');\n document.location = 'lihat_kategori.php'</script>";
  } else {
   echo "<script>alert('Terdapat Kesalahan dalam pembaruan data. Kode Error: '".mysqli_error($kon).");\n document.location = 'lihat_kategori.php'</script>";
  }
 }
}
include('header.php');
?>
<form method="POST" action="form_kategori.php" name="kategori" class="form-horizontal">
  <input type="hidden" name="id_lama" value="<?= isset($row['id'])?$row['id']:'' ?>" />
  <div class="form-group">
   <div class="col-sm-4">
    <label class="control-label" for="id_kategori">Nomor ID Kategori</label>
   </div>
   <div class="col-sm-8">
    <input type="text" name="id" id="id_kategori" class="form-control" value="<?= isset($row['id'])?$row['id']:'' ?>" placeholder="Masukkan ID Kategori" maxlength="5" required />
   </div>
  </div>
  <div class="form-group">
   <div class="col-sm-4">
    <label class="control-label" for="nama_kategori">Nama Kategori</label>
   </div>
   <div class="col-sm-8">
    <input type="text" name="nama_kategori" id="nama_kategori" class="form-control" value="<?= isset($row['nama_kategori'])?$row['nama_kategori']:'' ?>" placeholder="Masukkan Nama Kategori" maxlength="35" required />
   </div>
  </div>
  
  <div class="form-group">
   <div class="col-sm-12">
    <button type="submit" class="form-control btn btn-login" name="simpan">Simpan</button>
   </div>
  </div>
</form>
<?php
include('footer.php');