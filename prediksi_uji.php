<?php
include_once('config/koneksi.php');

$config['judul_sub_halaman'] = "<span class='fa fa-refresh'></span> Klasifikasi Data Uji";
$config['hal_aktif'] = "uji";
//hak_akses([1],TRUE);


$query = mysqli_query($kon, "select * from data_uji");
while($row = mysqli_fetch_assoc($query)){
 $data_uji['tweet'][] = $row['isi_tweet'];
 $data_uji['kelas'][] = $row['kelas'];
}

ob_start();
?>
<link href="assets/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
<link href="assets/css/jquery.dataTables_themeroller.min.css" rel="stylesheet"/>
<?php
$vws->set_inline(ob_get_clean());
include('header.php');
$vws->reset_inline();
?>
<div class="col-sm-12">
 <h3>Hasil Klasifikasi Data Uji</h3>
 <div class="well">
  <div class="text-center">
  <strong>Indikator Proses</strong>
  <div class="progress" style="margin-bottom:0px">
    <div class="progress-bar progress-bar-striped active" role="progressbar" id="prosespersen"
    aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
    </div>
  </div>
  <div id="prosespersen_text" style="margin-bottom: 15px">Siap Menjalankan Pengujian</div>
  </div>
  <table class="table table-striped table-responsive">
   <tr>
    <th>Waktu Mulai Pengujian</th>
    <td><span id="time_start">--</span></td>
    <th>Waktu Selesai Pengujian</th>
    <td><span id="time_finish">--</span> (<span id="time_elapsed">--</span> menit)</td>
   </tr>
   <tr>
    <th>Jumlah Data Latih</th>
    <td><span id="latih_count">--</span> tweet</td>
    <th>Jumlah Data Uji</th>
    <td><span id="uji_count">--</span> tweet</td>
   </tr>
  </table>
  <table class="table table-striped" id="accuracy_table">
   <thead>
   </thead>
   <tbody>
   </tbody>
  </table>
  <table class="table table-striped" id="newK_table">
   <thead>
   </thead>
   <tbody>
   </tbody>
  </table>
 </div>

 <table class="table table-striped table-bordered table-responsive" id="bukukita">
  <thead>
   <tr>
    <th>Tweet</th>
    <th>Kategori Asli</th>
   </tr>
  </thead>
  <tbody>
   <tr>
    <td colspan="5">Tidak Ada Data</td>
   </tr>
  </tbody>
 </table>
</div>
<?php
ob_start();
?>
<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/dataTables.bootstrap.min.js"></script>
<script>
function kelas(kls){
 var arr = ['Manusia dan Peristiwa','Politik','Olahraga'];
 return arr[kls];
}
</script>
<script>
 $(document).ready(function() {
  //$("#bukukita").DataTable();
  time_start = new Date();
  $("#time_start").text(time_start.toTimeString().split(' ')[0]);
  $.ajax({
   url: 'ajax_uji.php',
   method: 'GET',
   xhrFields: {
    onprogress: function(e){
     var response = e.currentTarget.response;
     allMessage = response.split("\n");
     lastMessage = JSON.parse(allMessage[allMessage.length - 2]);
     //console.log(lastMessage);
     if(lastMessage.message.hasOwnProperty('latih_count')){
      $("#latih_count").text(lastMessage.message.latih_count);
      $("#uji_count").text(lastMessage.message.uji_count);
      lastMessage.message.K_value.forEach(function(e,i) {
       $("#bukukita>thead>tr").append("<th>K="+e+"</th>");
      });
      $("#bukukita>tbody").html("");
     }
     if(lastMessage.message.hasOwnProperty('tweet')){
      htmlToAppend = "<tr><td>"+lastMessage.message.tweet+"</td><td>"+kelas(lastMessage.message.kelas)+"</td>";
      Object.keys(lastMessage.message.prediksi).forEach(function(e) {
       htmlToAppend += "<td>"+kelas(lastMessage.message.prediksi[e])+"</td>";
      });
      $("#bukukita>tbody").append(htmlToAppend);
     }
     if(lastMessage.message.hasOwnProperty('akurasi_K')){
      htmlToAppend = "";
      //$("#skor_hasil").html("");
      
      /* Akurasi Pengujian */
      $("#accuracy_table>thead").html("<tr><th>Akurasi</th></tr>");
      $("#accuracy_table>tbody").append("<tr id='precision_row'><th>Precision</th></tr>");
      $("#accuracy_table>tbody").append("<tr id='recall_row'><th>Recall</th></tr>");
      $("#accuracy_table>tbody").append("<tr id='fscore_row'><th>F-1 Score</th></tr>");
      Object.keys(lastMessage.message.akurasi_K).forEach(function(e) {
       $("#accuracy_table>thead>tr").append("<th>K-"+e+"</th>");
       skor_K = lastMessage.message.akurasi_K[e];
       $("#precision_row").append("<td>"+skor_K['precision']+"</td>");
       $("#recall_row").append("<td>"+skor_K['recall']+"</td>");
       $("#fscore_row").append("<td>"+skor_K['f1score']+"</td>");
      });
      
      /* Nilai K Baru */
      $("#newK_table>thead").html("<tr><th>Nilai K-Baru</th></tr>");
      $("#newK_table>tbody").append("<tr id='cls1_row'><th>"+kelas(0)+"</th></tr>");
      $("#newK_table>tbody").append("<tr id='cls2_row'><th>"+kelas(1)+"</th></tr>");
      $("#newK_table>tbody").append("<tr id='cls3_row'><th>"+kelas(2)+"</th></tr>");
      Object.keys(lastMessage.message.K_baru).forEach(function(e) {
       $("#newK_table>thead>tr").append("<th>K-"+e+"</th>");
       new_K = lastMessage.message.K_baru[e];
       $("#cls1_row").append("<td>"+new_K[0]+"</td>");
       $("#cls2_row").append("<td>"+new_K[1]+"</td>");
       $("#cls3_row").append("<td>"+new_K[2]+"</td>");
      });
      
      //$("#skor_hasil").append(htmlToAppend);
      $("#time_elapsed").text(lastMessage.message.time_elapsed);
      timefinish = new Date();
      $("#time_finish").text(timefinish.toTimeString().split(' ')[0]);
     }
     $("#prosespersen_text").html(lastMessage.message.progress_msg);
     $("#prosespersen").css('width',lastMessage.message.progress+'%');
    },
    onreadystatechange: function(){
     if(this.readyState == 4){
      $("#prosespersen").removeClass('active');
      console.log("DONE!");
     }
    }
   },
   complete: function(e,s){
    console.log(s);
    if(s == 'success'){
     $("#prosespersen_text").html("Proses Klasifikasi Selesai");
    } else if(s == 'error' || s == 'parsererror') {
     $("#prosespersen_text").html("ERROR! Terdapat masalah koneksi ke server");
    }
   }
  });
 });
</script>
<?php
$vws->set_inline(ob_get_clean());
include('footer.php');

